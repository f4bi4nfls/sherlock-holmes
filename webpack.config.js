const path = require('path');
const devMode = process.env.NODE_ENV === 'production';
const HtmlWebpackPlugin =  require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const buildPath = path.resolve(__dirname, 'dist');
module.exports = () =>  {

  const plugins = [
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    })
  ];

  plugins.push(
    new HtmlWebpackPlugin({
      template:path.join(__dirname,'./src/index.tpl.html'),
      filename:'index.html',
      path:buildPath
    })
  );

  return {
    entry : {
      vendor:['react','react-dom'],
      web:path.resolve(__dirname,'./src/index.js')
    },
    output : {
      path: buildPath,
      filename: 'js/[name].[hash].js',
      chunkFilename: 'js/[name].[chunkhash].js'

    },
    devServer: {
      port: process.env.DEV_SERVER_PORT || 9000,
      historyApiFallback: true
    },
    module : {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-react'],
              plugins:["react-hot-loader/babel"]
            }
          }
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf|OTF|TTF)$/,
          use: 'file-loader'
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.scss$/,
          use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
            "sass-loader" // compiles Sass to CSS, using Node Sass by default
          ]
        }
      ]
    },
    mode:'development',
    plugins,
    optimization: {
      splitChunks: {
        cacheGroups: {
          vendor: {
            name: 'vendor',
            chunks: 'initial',
            minChunks: Infinity,
            enforce: true
          }
        }
      }
    }

  }
};

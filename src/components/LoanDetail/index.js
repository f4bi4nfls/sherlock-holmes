import React  from 'react';
import './LoanDetail.scss'
const renderPayments = ( ) => {
    const arr = [1,2,3,4,5,6,7];
   return arr.map(num => {
        return (  <tr className="itemContainerDetail">
            <td className="tdLoan ">
                <div className="flexTdLoan">
                    <span className="spanTd"></span>
                    <span className="spanTd spanLoanId"></span>
                </div>
            </td>
            <td className="tdLoan">
                <div className="flexTdLoan">
                    <span className="spanTd"></span>
                    <span className="spanTd spanLoanId"></span>
                </div>
            </td>
            <td className="tdLoan">
                <span className="spanTd"></span>
            </td>

        </tr>)
    })
}
const LoanDetail = () => {
    return (
        <div className="containerList">
            <h1 className="LoansList">Detalle del Prestamo</h1>
            <div className="containerInfo">
                <div className="info">
                    <h4>Client Name</h4>
                </div>
                <div className="info">
                    <h4>debt</h4>
                </div>
            </div>
            <div className="detail">
                <h3>Pagos</h3>
                <table className="tablePayments">
                    <thead>
                    <tr>
                        <th className="headerTitle">Fecha</th>
                        <th className="headerTitle">Monto</th>
                        <th className="headerTitle">Principal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {renderPayments()}
                    </tbody>
                </table>
            </div>
        </div>
    )
};

export default LoanDetail;
// Libraries
import React, {Component} from 'react'
import data from '../../data'
import './LoansList.scss';
class LoanList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loans:[]
        }
    }

    componentDidMount() {
        const {loans} = data;
        this.setState({loans});
    }
    renderStatus(status) {
        const color = {backgroundColor:'#000000',color:'#000000'};
        let text = '';
        switch (status) {
            case 'on-time':
                color.backgroundColor='#d2f0cf';
                color.color = '#21882b';
                text='a tiempo';
                break;
            case 'behind-2':
                color.backgroundColor='#ffdec2';
                color.color = '#dc5e31';
                text='retraso 31-60';
                break;
            case 'cancelled':
                color.backgroundColor='#ffd5d5';
                color.color = '#bf3716';
                text='cancelado';
                break;
            case 'behind-1':
                color.backgroundColor='#ffdec2';
                color.color = '#c98201';
                text='retraso 1-30';
                break;
            case 'completed':
                color.backgroundColor='#d2f0cf';
                color.color = '#21882b';
                text='completado';
                break;
            case 'default':
                color.backgroundColor='#ffd5d5';
                color.color = '#bf3716';
                text='default';
                break

        }
        return (
            <span className="statusContainer" style={color}>{text}</span>
        )
    }
    renderLoans() {
        const {loans} = this.state;

       return loans.map(loan => {

            const {name,email} = loan.customer;
           const {id,loan_amount,start_date,status} = loan;
           const interest_rate = loan.interest_rate * 100;
            return (

                    <tr key={email} className="itemContainer">
                        <td className="tdLoan ">
                            <div className="flexTdLoan">
                                <span className="spanTd">{name}</span>
                                <span className="spanTd spanLoanId">{id}</span>
                            </div>
                        </td>
                        <td className="tdLoan">
                            <div className="flexTdLoan">
                                <span className="spanTd">${(loan_amount.toLocaleString())}</span>
                                <span className="spanTd spanLoanId">{interest_rate}%</span>
                            </div>
                        </td>
                        <td className="tdLoan">
                            <span className="spanTd">{start_date}</span>
                        </td>
                        <td className="tdLoan textAlignRight ">
                            {this.renderStatus(status)}
                        </td>
                    </tr>
            );
        });
    }
    render() {
        return (
            <div className="containerList">
                <h1 className="LoansList">Listado de Prestamos</h1>
                <div className="headerContainer">
                  <table className="tableLoans">

                     <thead>
                     <tr>
                         <th className="headerTitle">Cliente</th>
                         <th className="headerTitle">Préstamo</th>
                         <th className="headerTitle">Fecha</th>
                         <th className="headerTitle textAlignRight">Estado</th>
                     </tr>
                     </thead>
                      <tbody>
                      {this.renderLoans()}
                      </tbody>
                  </table>
                </div>
            </div>
        )
    }
}


export default LoanList

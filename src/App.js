import React,{Component} from 'react';
import {hot} from "react-hot-loader/root";
import LoanList from './components/LoanList';
import LoanDetail from './components/LoanDetail';
import  './App.scss';
class App extends Component {
  render() {
    return(
        <LoanList />
    )
  }
}

export default hot(App);

# Loans app 🤓

Test app

## Installation

Use the node package manager to install 
Use the package manager [npm](https://www.npmjs.com/) to install.

```bash
npm install
```

## Run development server

```bash
npm start
```



## License
[MIT](https://choosealicense.com/licenses/mit/)
